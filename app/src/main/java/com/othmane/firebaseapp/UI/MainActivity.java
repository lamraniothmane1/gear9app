package com.othmane.firebaseapp.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.othmane.firebaseapp.R;
import com.othmane.firebaseapp.adapter.MessageAdapter;
import com.othmane.firebaseapp.model.Message;
import com.othmane.firebaseapp.presenter.MessageListPresenter;
import com.othmane.firebaseapp.view.IMessageView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements IMessageView {

    private static final String TAG = "MainActivity.class";
    private EditText et_message;
    private MessageListPresenter messagePresenter;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private MessageAdapter adapter;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Init view
        initView();

        // MessagePresenter
        messagePresenter = new MessageListPresenter(this, this);
        // Load messages from Firebase
        messagePresenter.onLoadMesaages();

        // Send Button click listener
        findViewById(R.id.btn_add_message).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage();
            }
        });

        // On ok pressed on keyboard => Send message
        et_message.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    Log.i(TAG,"Enter pressed");
                    sendMessage();
                }
                return false;
            }
        });

        // Configure the swipe refresh layout
        configSwipeRefreshLayout();
    }

    /**
     * Configure the Swipe refresh layout
     */
    private void configSwipeRefreshLayout() {
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // remove the old items
                adapter.removeAllItems();
                // load the new items
                messagePresenter.onLoadMesaages();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    /**
     * Sending a message
     */
    private void sendMessage() {
        // setting values
        String st_message = et_message.getText().toString();
        String deviceName = android.os.Build.MODEL;
        String currentDate = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault()).format(new Date());

        Message message = new Message();
        message.setContent(st_message);
        message.setName(deviceName);
        message.setTimeStamp(currentDate);

        // Sending the message
        messagePresenter.onAddMessage(message);
    }

    private void initView() {
        et_message = findViewById(R.id.et_message);
        init_recycler_view();
    }

    /**
     * Initialize the recyclerview for messages
     */
    private void init_recycler_view() {
        recyclerView = findViewById(R.id.recyclerView_messages);
        // To make scrolling smoothly
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new MessageAdapter(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onDataAdded(Message message) {
        adapter.addItem(message);
    }

    @Override
    public void onDataChanged(Message message) {
        adapter.updateItem(message);
    }

    @Override
    public void onDataRemoved(String messageKey) {
        adapter.removeItem(messageKey);
    }

    @Override
    public void onUpdateEditText() {
        et_message.setText("");
    }

    @Override
    public void onUnavailableConnection() {
        Toast.makeText(this, R.string.network_unavailable, Toast.LENGTH_SHORT).show();
    }


    /**
     * Show fragment
     */
    public void showFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_frame, fragment, fragment.getClass().getName());
        fragmentTransaction.addToBackStack(fragment.getTag());
        fragmentTransaction.commit();
    }

    /**
     * Hide all fragments
     */
    public void hideFragments(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        for(Fragment fragment : fragments){
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.remove(fragment);
            fragmentTransaction.commit();
        }
    }

}
